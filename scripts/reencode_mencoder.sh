#!/bin/sh
set -x

INPUT_FILE='ffout.mkv'
OUTPUT_FILE='out.avi'

# The inverse of what "smooth-dl -s" tells us
AV_DELAY="-0.441333"

VIDEO_BITRATE="1600"
AUDIO_BITRATE="192"

OPTIONS="-noskip -vf harddup"
AV_SYNC="-audio-delay $AV_DELAY"

mencoder $INPUT_FILE -o /dev/null $AV_SYNC -ovc x264 \
-x264encopts pass=1:bitrate=${VIDEO_BITRATE}:bframes=1:\
me=umh:partitions=all:trellis=1:qp_step=4:qcomp=0.7:direct_pred=auto:keyint=300:threads=auto \
${OPTIONS} \
-oac mp3lame -lameopts cbr:preset=$AUDIO_BITRATE -channels 2 -srate 48000
# faac still gives some problems with open video editors
#-oac faac -faacopts br=${AUDIO_BITRATE}:mpeg=4:object=2 -channels 2 -srate 48000

mencoder $INPUT_FILE -o $OUTPUT_FILE $AV_SYNC -ovc x264 \
-x264encopts pass=2:bitrate=${VIDEO_BITRATE}:frameref=5:bframes=1:\
me=umh:partitions=all:trellis=1:qp_step=4:qcomp=0.7:direct_pred=auto:keyint=300:threads=auto \
${OPTIONS} \
-oac mp3lame -lameopts cbr:preset=$AUDIO_BITRATE -channels 2 -srate 48000
# faac still gives some problems with open video editors
#-oac faac -faacopts br=${AUDIO_BITRATE}:mpeg=4:object=2 -channels 2 -srate 48000
