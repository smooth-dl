#!/bin/sh

INPUT_FILE="ffout.mkv"
OUTPUT_FILE="out.mp4"

# The inverse of what "smooth-dl -s" tells us
AV_DELAY="-0.441333"

VIDEO_BITRATE="1500k"
AUDIO_BITRATE="128k"

VIDEO_OPTIONS="-g 100 -keyint_min 50 -flags2 -mbtree"
AV_SYNC="-itsoffset $AV_DELAY -i $INPUT_FILE -map 1:0 -map 0:1"

AUDIO_NONE="-an"
AUDIO_OPTIONS="-acodec libmp3lame -ac 2 -ab $AUDIO_BITRATE"
VIDEO_PASS1="-vcodec libx264 -vpre veryfast_firstpass ${VIDEO_OPTIONS}"
VIDEO_PASS2=-"vcodec libx264 -vpre veryfast ${VIDEO_OPTIONS}"

# iPhone compatible streams
VIDEO_PASS1="$VIDEO_PASS1 -vpre baseline"
VIDEO_PASS2="$VIDEO_PASS2 -vpre baseline"

echo $INPUT_FILE $OUTPUT_FILE $AUDIO_NONE $VIDEO_PASS1

#LANG=C ffmpeg -y  -i $INPUT_FILE $AV_SYNC $ADIO_NONE     $VIDEO_PASS1 -pass 1 -b 1500k $OUTPUT_FILE
LANG=C ffmpeg -y  -i $INPUT_FILE $AV_SYNC $AUDIO_OPTIONS $VIDEO_PASS1 -pass 1 -b $VIDEO_BITRATE $OUTPUT_FILE
LANG=C ffmpeg -y  -i $INPUT_FILE $AV_SYNC $AUDIO_OPTIONS $VIDEO_PASS2 -pass 2 -b $VIDEO_BITRATE $OUTPUT_FILE
