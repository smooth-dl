// SilverSpoof - spoof Silverlight client installation
//
// Copyright (C) 2010  Antonio Ospite <ospite@studenti.unina.it>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// --------------------------------------------------------------------
//
// This is a Greasemonkey user script.  To install it, you need
// Greasemonkey 0.3 or later: http://greasemonkey.mozdev.org/
// Then restart Firefox and revisit this script.
// Under Tools, there will be a new menu item to "Install User Script".
// Accept the default configuration and install.
//
// To uninstall, go to Tools/Manage User Scripts,
// select "SilverSpoof", and click Uninstall.
//
// --------------------------------------------------------------------
//
// This script spoofs a Silverlight installation, allowing the User Agent to
// acess the <object></object> element relative to some Silverlight content
// even if Silverlight (or Moonlight) is not installed.
//
// ==UserScript==
// @name          SilverSpoof
// @namespace     http://ao2.it
// @description   Script to spoof Silverlight client installation
// ==/UserScript==

unsafeWindow.Silverlight.isInstalled = function(version) {
  return true;
};
